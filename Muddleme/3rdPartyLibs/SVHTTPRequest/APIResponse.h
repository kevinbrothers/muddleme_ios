//
//  APIResponse.h
//
//  Created on 27/09/2013.

//

#import <Foundation/Foundation.h>
#import "Error.h"

@interface APIResponse : NSObject

@property (strong, nonatomic) Error *error;
@property (strong, nonatomic) NSString *optionalMessage;
@property (strong, nonatomic) id data;

+ (APIResponse *)handleResponse:(NSDictionary *)dictResponse error:(NSError *)networkError;

@end
