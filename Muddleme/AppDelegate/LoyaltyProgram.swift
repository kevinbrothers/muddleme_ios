//
//  LoyaltyProgram.swift
//  Muddleme
//
//  Created by Arkhitech on 18/12/2015.
//  Copyright © 2015 AESquares. All rights reserved.
//

import UIKit

class LoyaltyProgram: NSObject {
    
    var Name:String=String()
    var image:String=String()
    var PId:Int=Int()
    var bIsJoined = false
    
    var CoupansCount:Int=Int()
    var PersonalOffersCount:Int=Int()

    var account:String=String()

       func InintWithDic(dict:NSDictionary) {
        
        
        
        
        if(self.valueForKeyExist(dict, key: "name") == true){
            self.Name = dict.valueForKey("name") as! String
        }
        
        
        
        if(self.valueForKeyExist(dict, key: "loyalty_program_api_params") == true)
        {
            
        let extraDict = dict.valueForKey("loyalty_program_api_params") as! NSDictionary
            
            
            if(self.valueForKeyExist(extraDict, key: "logo_image_url") == true){
                
                self.image = extraDict.valueForKey("logo_image_url") as! String
            }
            
            
            
            
            if(self.valueForKeyExist(extraDict, key: "coupons_count") == true){
                self.CoupansCount = (extraDict.valueForKey("coupons_count") as! Int)
            }
            if(self.valueForKeyExist(extraDict, key: "personal_offers_count") == true){
                self.PersonalOffersCount = (extraDict.valueForKey("personal_offers_count") as! Int)
            }
            
            
            
            
            
            
        }
        
        
        
        
        
        
        
        
        if(self.valueForKeyExist(dict, key: "id") == true){
            self.PId = (dict.valueForKey("id") as! Int)
        }
        
        if(self.valueForKeyExist(dict, key: "account_number") == true){
            
            self.account = dict.valueForKey("account_number") as! String
        }
        
}
    
    func InintWithDic2(dict2:NSDictionary) {
        
        if(self.valueForKeyExist(dict2, key: "loyalty_program"))
        {
            
        }else
        {
            return;
        }
        
        let dict:NSDictionary =  dict2.valueForKey("loyalty_program") as! NSDictionary
        
        if(self.valueForKeyExist(dict, key: "name") == true){
            self.Name = dict.valueForKey("name") as! String
        }
        if(self.valueForKeyExist(dict, key: "loyalty_program_api_params") == true)
        {
            
            let extraDict = dict.valueForKey("loyalty_program_api_params") as! NSDictionary
            
            
            if(self.valueForKeyExist(extraDict, key: "logo_image_url") == true){
                
                self.image = extraDict.valueForKey("logo_image_url") as! String
            }
            
            if(self.valueForKeyExist(extraDict, key: "coupons_count") == true){
                self.CoupansCount = (extraDict.valueForKey("coupons_count") as! Int)
            }
            if(self.valueForKeyExist(extraDict, key: "personal_offers_count") == true){
                self.PersonalOffersCount = (extraDict.valueForKey("personal_offers_count") as! Int)
            }
            
        }
        if(self.valueForKeyExist(dict, key: "id") == true){
            self.PId = (dict.valueForKey("id") as! Int)
        }
        
        if(self.valueForKeyExist(dict2, key: "account_number") == true){
            
            self.account = dict2.valueForKey("account_number") as! String
        }
        
    }

    
    func valueForKeyExist(dict:NSDictionary, key:String)->Bool
    {
        if(dict.valueForKey(key) != nil)
        {
            let value:AnyObject = dict.valueForKey(key)!
            if(value.isKindOfClass(NSNull) == true)
            {
                return false
            }
            return true
        }
        
        return false
    }
    


}
