//
//  User.swift
//  OnTrack
//
//  Created by Arkhitech on 28/07/2015.
//  Copyright (c) 2015 Arkhitech. All rights reserved.
//

import UIKit

class User: NSObject {
    
    var firstName:String=String()
    var lastName:String=String()
    var age:String=String()
    var email:String=String()
    var authenticationToken:String=String()
    var uid:Int=Int()
    
    var address:String=String()
    var balance:Int=Int()
    var total_balance:Int=Int()
    var pending_outcome:Int=Int()
    var city:String=String()
    var zipCode:String=String()
   
    static var sharedUser:User?
    
    static func currentUser() -> User {
       
        let user:User = User()
    
        return user;
    }
    
    
    
    static func sharedInstance() -> User {
        
        if(sharedUser == nil )
        {
          sharedUser = User.currentUser()
        }
        
        return sharedUser!
        
        
    }

    
    func InintWithDic(dict:NSDictionary) {
        
        
        
        
        if(self.valueForKeyExist(dict, key: "first_name") == true){
            self.firstName = dict.valueForKey("first_name") as! String
        }
        if(self.valueForKeyExist(dict, key: "last_name") == true){
            
            self.lastName = dict.valueForKey("last_name") as! String
        }
        if(self.valueForKeyExist(dict, key: "age_range") == true){
            self.age = (dict.valueForKey("age_range") as! String)
        }
        if(self.valueForKeyExist(dict, key: "balance") == true){
            self.balance = dict.valueForKey("balance") as! Int
        }
        if(self.valueForKeyExist(dict, key: "email") == true){
            
            self.email = dict.valueForKey("email") as! String
        }
        
        if(self.valueForKeyExist(dict, key: "city") == true){
            
            self.city = dict.valueForKey("city") as! String
        }
       
        if(self.valueForKeyExist(dict, key: "pending_outcome") == true){
            self.pending_outcome = dict.valueForKey("pending_outcome") as! Int
        }
        if(self.valueForKeyExist(dict, key: "total_balance") == true){
            self.total_balance = dict.valueForKey("total_balance") as! Int
        }
    }
    
    
    func valueForKeyExist(dict:NSDictionary, key:String)->Bool
    {
        if(dict.valueForKey(key) != nil)
        {
            let value:AnyObject = dict.valueForKey(key)!
            if(value.isKindOfClass(NSNull) == true)
            {
                return false
            }
            return true
        }
        
        return false
    }
    
    
  }
