//
//  AccountBalanceViewController.m
//  Muddleme
//
//  Created by Asad Ali on 09/08/2015.
//  Copyright (c) 2015 AESquares. All rights reserved.
//

#import "AccountBalanceViewController.h"

@interface AccountBalanceViewController ()
@property (weak, nonatomic) IBOutlet UIView *containerView;
@end

@implementation AccountBalanceViewController



#pragma mark - UIViewController Life Cycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    
    [self setupNavigationBarWithMenuForTitle:@"Account Balance"];
    [self.containerView roundTheCornersWithBorder:4.0];
    self.bgView1.layer.cornerRadius = 10;
    self.bgView1.layer.borderWidth = 2;
    self.bgView1.layer.borderColor = [UIColor whiteColor].CGColor;
    self.bgView2.layer.cornerRadius = 10;
    self.bgView2.layer.borderWidth = 1;
    self.bgView2.layer.borderColor = [UIColor whiteColor].CGColor;
    self.bgView3.layer.cornerRadius = 10;
    self.bgView3.layer.borderWidth = 1;
    self.bgView3.layer.borderColor = [UIColor colorWithRed:195.0/255.0 green:91.0/255.0 blue:41.0/255.0 alpha:1.0].CGColor;
    
    self.bgView4.layer.cornerRadius = 10;
    self.bgView4.layer.borderWidth = 1;
    self.bgView4.layer.borderColor = [UIColor whiteColor].CGColor;
    
    self.bgView5.layer.cornerRadius = 10;
    self.bgView5.layer.borderWidth = 1;
    self.bgView5.layer.borderColor = [UIColor whiteColor].CGColor;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


<<<<<<< HEAD
=======
- (IBAction)WithdrawFunds:(id)sender
{
    if ([self isFormValid])
    {
        [SVProgressHUD show];
        NSMutableDictionary* params = [[NSMutableDictionary alloc] init];
        
        [params setObject:[NSString stringWithFormat:@"%@",self.ValTextField.text] forKey:@"funds_withdrawal[amount]"];
        [params setObject:[NSString stringWithFormat:@"%@",self.emailTextField.text] forKey:@"funds_withdrawal[paypal_email]"];
        
       // NSString* url = [NSString stringWithFormat:@"/loyalty_programs/%li/coupons",self.currentProgram.PId];
        
        [[UtilityFunctions authenticatedHTTPClient] POST:@"funds_withdrawal" parameters:params completion:^(id response, NSHTTPURLResponse *urlResponse, NSError *error)
         {
             if (!error)
             {
                 NSString* result= response[@"resulting_balance"];
                 self.availableBalLabel.text = result;
                 
                 User * user = [User sharedInstance];

                 
                 user.balance = (long)result;
                 
                 
                 NSDictionary* dict = [UtilityFunctions getValueFromUserDefaultsForKey:@"user"];
                 
                 [dict setValue:[NSString stringWithFormat:@"$%li",(long)user.balance] forKey:@"balance"];
                 
                 [UtilityFunctions setValueInUserDefaults:dict forKey:@"user"];

                 
                 
             }else
             {
                 [self showAlertViewWithTitle:@"" message:response[@"error"]];
             }
             [SVProgressHUD dismiss];
        }];
    }
}
>>>>>>> 1b55bff... updated coupan view

#pragma mark - Buttons Action


@end
