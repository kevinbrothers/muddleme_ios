//
//  MyAccountTableViewCell.h
//  Muddleme
//
//  Created by Asad Ali on 10/08/2015.
//  Copyright (c) 2015 AESquares. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyAccountTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblValue;
@property (weak, nonatomic) IBOutlet UIView *bgView;


@end
