//
//  MyAccountTableViewCell.m
//  Muddleme
//
//  Created by Asad Ali on 10/08/2015.
//  Copyright (c) 2015 AESquares. All rights reserved.
//

#import "MyAccountTableViewCell.h"

@implementation MyAccountTableViewCell

-(void)awakeFromNib{
    self.bgView.layer.cornerRadius = 10;
    self.bgView.layer.borderWidth = 2;
    self.bgView.layer.borderColor = [UIColor whiteColor].CGColor;

}

@end
