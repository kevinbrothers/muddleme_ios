//
//  AuctionArchiveViewController.m
//  Muddleme
//
//  Created by Asad Ali on 09/08/2015.
//  Copyright (c) 2015 AESquares. All rights reserved.
//

#import "AuctionArchiveViewController.h"
#import "PersonalOfferCollectionViewCell.h"
#import <MediaPlayer/MediaPlayer.h>

@interface AuctionArchiveViewController ()
@property (weak, nonatomic) IBOutlet UIView *containerView1;
@property (weak, nonatomic) IBOutlet UIView *containerView2;
@property (weak, nonatomic) IBOutlet UIView *containerView3;

@property (weak, nonatomic) IBOutlet UILabel *lblMerchantName;
@property (weak, nonatomic) IBOutlet UILabel *lblUserFullName;
@property (weak, nonatomic) IBOutlet UILabel *lblAccountNumber;

@property (strong, nonatomic) NSMutableArray *offers;
@property (weak, nonatomic) IBOutlet UICollectionView *offersCollectionView;


@end

@implementation AuctionArchiveViewController



#pragma mark - UIViewController Life Cycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    [self setupNavigationBarWithMenuForTitle:@"Muddle M"];
    [self.containerView1 roundTheCornersWithBorder:4.0];
    [self.containerView2 roundTheCornersWithBorder:4.0];
    self.containerView3.layer.cornerRadius = 10;
    self.containerView3.layer.borderWidth = 1;
    self.containerView3.layer.borderColor = [UIColor whiteColor].CGColor;
    
    [self.offersCollectionView registerNib:[UINib nibWithNibName:@"PersonalOfferCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"OfferCollectionViewCell"];

    
    NSDictionary *userInfo = [UtilityFunctions getValueFromUserDefaultsForKey:@"user"];
    
    
    self.lblUserFullName.text = userInfo[@"first_name"];
    self.lblMerchantName.text = self.currentProgram.Name;
    self.lblAccountNumber.text = self.currentProgram.account;
   // self.lblAccountNumber.text =userInfo[@"email"];
    
    [self getOffersForMerchant:nil];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)getOffersForMerchant:(NSString *)merchantId
{
    [SVProgressHUD show];
    NSMutableDictionary* params = [[NSMutableDictionary alloc] init];
    
    [params setObject:[NSString stringWithFormat:@"%li",self.currentProgram.PId] forKey:@"loyalty_program_id"];
    
<<<<<<< HEAD
    [[UtilityFunctions authenticatedHTTPClient] GET:@"loyalty_programs/offers" parameters:params completion:^(id response, NSHTTPURLResponse *urlResponse, NSError *error)
=======
   // NSString* url = [NSString stringWithFormat:@"loyalty_programs/%li/offers",self.currentProgram.PId];
    NSString* url = [NSString stringWithFormat:@"loyalty_programs/%li/personal_offers",self.currentProgram.PId];

    
    
    
    [[UtilityFunctions authenticatedHTTPClient] GET:url parameters:nil completion:^(id response, NSHTTPURLResponse *urlResponse, NSError *error)
>>>>>>> 1b55bff... updated coupan view
     {
         [SVProgressHUD dismiss];
         
         self.offers = response;
<<<<<<< HEAD
         
//         self.offers = [[NSMutableArray alloc ]init];
//         for (NSDictionary* dict in response)  {
//             
//             Coupon* programm = [[Coupon alloc] init];
//             [programm InintWithDic:dict];
//             [self.offers addObject:programm];
//         }
         
=======
>>>>>>> 1b55bff... updated coupan view
         
//         self.offers = [[NSMutableArray alloc ]init];
//         
//         NSArray* arr1 = [response valueForKey:@"avant_offers"];
//         for (NSDictionary* dict in arr1)  {
//             
//             
//             [self.offers addObject:dict];
//         }
//         NSArray* arr2 = [response valueForKey:@"cj_offers"];
//         for (NSDictionary* dict in arr2)  {
//             
//             
//             [self.offers addObject:dict];
//         }
//         NSArray* arr3 = [response valueForKey:@"loyalty_program_offers"];
//         for (NSDictionary* dict in arr3)  {
//             
//            
//             [self.offers addObject:dict];
//         }
//         
//         
         if (self.offers.count > 0) [self.offersCollectionView reloadData];
         else [self showAlertViewWithTitle:kEmptyString message:@"No offer found"];
     }];
}

#pragma mark - Buttons Action


#pragma mark - UICollectionViewDataSource

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    PersonalOfferCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"OfferCollectionViewCell" forIndexPath:indexPath];
    
   NSDictionary* dict = self.offers[indexPath.row];
    
    
<<<<<<< HEAD
    cell.lblOfferHeader.text = dict[@"offer_description"];
    cell.lblExpireDate.text = [NSString stringWithFormat:@"This expires at %@",dict[@"expiration_time"]];
      NSString* url1 = [NSString stringWithFormat:@"http://104.197.68.95/%@",dict[@"offer_video_url"]];
    [cell embedVideo:url1];
    cell.layer.cornerRadius = 5.0;
=======
//    cell.lblOfferHeader.text = dict[@"offer_description"];
//    cell.lblExpireDate.text = [NSString stringWithFormat:@"This expires at %@",dict[@"expiration_time"]];
    
    
    cell.lblOfferHeader.text = dict[@"header"];
    cell.lblExpireDate.text = [NSString stringWithFormat:@"This expires at %@",dict[@"expiration_date"]];
    NSDictionary* dict2 = dict[@"offer_url"];

    
    if (![[dict valueForKey:@"offer_video_file_size"] isKindOfClass:[NSNull class]])
    {
        NSString* url1 = [NSString stringWithFormat:@"http://104.197.68.95/%@",dict2[@"video_url"]];
        
        
        cell.movieURL = url1;
    }else
    {
        [cell.playBuuton setHidden:YES];
    }
    
    
    
    NSString* url2 = [NSString stringWithFormat:@"http://104.197.68.95/%@",dict2[@"offer_barcode_image_url"]];
    NSString* url3 = [NSString stringWithFormat:@"http://104.197.68.95/%@",dict2[@"offer_image_url"]];
    //[cell embedVideo:url1];
>>>>>>> 1b55bff... updated coupan view
    
    cell.barcodeView.layer.cornerRadius = 5.0;
    [cell.barcodeImageView sd_setImageWithURL:[NSURL URLWithString:url2] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL){
        }];
    
    [cell.vidImageView sd_setImageWithURL:[NSURL URLWithString:url3] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL){
    }];
    
    

    return cell;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.offers.count;
}



#pragma mark - UICollectionViewDelegateFlowLayout

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(self.offersCollectionView.bounds.size.width, 457);
}


- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    [self.view endEditing:YES];
    
 
    
    NSDictionary *itemDict = self.offers[indexPath.row];
    
    
    NSString* url1 = [NSString stringWithFormat:@"http://104.197.68.95/%@",itemDict[@"offer_video_url"]];
    NSURL *url=[[NSURL alloc] initWithString:url1];
    
    MPMoviePlayerViewController *moviePlayer=[[MPMoviePlayerViewController alloc] initWithContentURL:url];
    
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(moviePlayBackDidFinish:) name:MPMoviePlayerPlaybackDidFinishNotification object:moviePlayer];
//    
//    moviePlayer.controlStyle=MPMovieControlStyleDefault;
//    moviePlayer.shouldAutoplay=YES;
//   // [self.view addSubview:moviePlayer.view];
//    [moviePlayer setFullscreen:YES animated:YES];
    
    
    
    [self presentMoviePlayerViewControllerAnimated:moviePlayer];
}
- (void) moviePlayBackDidFinish:(NSNotification*)notification
{
    
    MPMoviePlayerController *player = [notification object];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:MPMoviePlayerPlaybackDidFinishNotification object:player];
    
    if ([player respondsToSelector:@selector(setFullscreen:animated:)])
    {
        [player.view removeFromSuperview];
    }
}
@end
