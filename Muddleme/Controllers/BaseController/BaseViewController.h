//
//  BaseViewController.h
//  Muddleme
//
//  Created by Asad Ali on 19/03/2015.
//  Copyright (c) 2015 AESquares. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "nuHelpers.h"
#import "AppProtocols.h"
#import "SVHTTPClient.h"
#import "UIImageView+WebCache.h"
#import "PureLayout.h"
#import "UtilityFunctions.h"


@interface BaseViewController : UIViewController


typedef enum {
    UINavigationBarRightButtonTypeMenu,
    UINavigationBarRightButtonTypeAddNew,
    UINavigationBarRightButtonTypeDone,
    UINavigationBarRightButtonTypeFilter,
    UINavigationBarRightButtonTypeLogout,
} UINavigationBarRightButtonType;




@property (nonatomic) id<BaseViewControllerDelegate> baseDelegate;


- (void)makeUINavigationBarTransparent;

- (void)setupNavigationBarWithProductLogo:(NSString *)logoURL showBackButtonIfNeeded:(BOOL)show;

- (void)setupNavigationBarLogoWithSecondaryImage:(NSString *)imageName showBackButtonIfNeeded:(BOOL)show;

- (void)setupNavigationBarWithTitleImage:(NSString *)imageURL showBackButtonIfNeeded:(BOOL)show;

- (void)setupNavigationBarWithMenuForTitle:(NSString *)title;

- (void)setupNavigationBarWithTitle:(NSString *)title showBackButtonIfNeeded:(BOOL)show;

- (void)showNavBarSecondaryImageWithRatio:(float)ratio;

- (void)showBackButtonIfNeeded:(BOOL)show;

- (void)setBackButtonImage:(NSString *)imageName;

- (void)showRightNavigationMenuButton;

- (void)showRightNavigationButtonWithType:(UINavigationBarRightButtonType)type;

- (void)setRightNavigationButtonSelected:(BOOL)selected;

- (void)hideRightNavigationButton;

- (void)hideNavigationBar:(BOOL)hide;

- (void)hideNavigationBar:(BOOL)hide animated:(BOOL)animated;

- (void)enableEdgePanGestureRecognizerToPopViewController:(BOOL)enable;

- (void)enableTopSwipeGestureRecognizer:(BOOL)enable;

- (void)logoutToLoginViewController;

- (void)animateRightNavigationButtonWithDegree:(int)degree;

@end
