//
//  BaseViewController.m
//  Muddleme
//
//  Created by Asad Ali on 19/03/2015.
//  Copyright (c) 2015 AESquares. All rights reserved.
//

#import "BaseViewController.h"
#import "MenuViewController.h"

@interface BaseViewController() <UIGestureRecognizerDelegate>


@property (strong, nonatomic) UIScreenEdgePanGestureRecognizer *edgePanGestureRecognizer;
@property (strong, nonatomic) UIButton *rightNavigationButton;
@property (strong, nonatomic) UIImageView *secondaryImageView;
@property (strong, nonatomic) UIImageView *logoImageView;
@property (strong, nonatomic) UIButton *leftNavButton;
@property (strong, nonatomic) UIImageView *profileImageView;

@property (strong, nonatomic) UISwipeGestureRecognizer *topSwipeGestureRecognizer;


@end


@implementation BaseViewController



#pragma mark - UIViewController Life Cycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
//    [self.navigationController.navigationBar setTranslucent:NO];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
}


- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    self.baseDelegate = nil;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    
    NSLog(@"\n\n---------- didReceiveMemoryWarning ----------\n\n");
}

- (void)dealloc
{
    NSLog(@"---------- dealloc ----------");
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (void)makeUINavigationBarTransparent
{
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    self.navigationController.navigationBar.translucent = YES;
}




#pragma mark - Custom Methods


- (void)setupNavigationBarWithMenuForTitle:(NSString *)title
{
    [self hideNavigationBar:NO];
    
    
    UIImage *backBarImage = UIImageFromName(kNavbarLogoImageName);
    
    self.leftNavButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, backBarImage.size.width, backBarImage.size.height)];
    
    [self.leftNavButton addTarget:self action:@selector(backButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.leftNavButton setImage:backBarImage forState:UIControlStateNormal];
    
    UIBarButtonItem *backbtnItem = [[UIBarButtonItem alloc] initWithCustomView:self.leftNavButton];
    
    [[self navigationItem] setLeftBarButtonItem:backbtnItem];
    
    
    [self showRightNavigationMenuButton];
    
    
    
    if (title != nil) self.navigationItem.title = title;
}

- (void)setupNavigationBarWithProductLogo:(NSString *)logoURL showBackButtonIfNeeded:(BOOL)show
{
    self.profileImageView = [[UIImageView alloc] initWithImage:PlaceHolderImage];
    self.profileImageView.layer.cornerRadius = 30;
    self.profileImageView.layer.masksToBounds = YES;
    self.profileImageView.frame = CGRectMake(-30, -20, 60, 60);
    UIImageView *barImageViewWorkAround = [[UIImageView alloc] init];
    
    [self.navigationItem setTitleView:barImageViewWorkAround];
    [barImageViewWorkAround addSubview:self.profileImageView];
    
    
    [self.profileImageView setImage:PlaceHolderImage];
    
    [self.profileImageView sd_setImageWithURL:[NSURL URLWithString:logoURL]];
    
    [self.profileImageView roundedView];
    

    [self showBackButtonIfNeeded:show];
}

- (void)setupNavigationBarLogoWithSecondaryImage:(NSString *)imageURL showBackButtonIfNeeded:(BOOL)show
{
    [self hideNavigationBar:NO];
    

    UIImage *logo = UIImageFromName(kNavbarLogoImageName);
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, logo.size.width, logo.size.height)];
    
    
    self.logoImageView = [[UIImageView alloc] initWithImage:UIImageFromName(kNavbarLogoImageName)];
    [self.logoImageView roundedView];
    
    self.secondaryImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 32, 32)];
    [self.secondaryImageView sd_setImageWithURL:[NSURL URLWithString:imageURL]];
    self.secondaryImageView.alpha = 0.0;
    
    
    [view addSubview:self.secondaryImageView];
    [view addSubview:self.logoImageView];
    
    [self.navigationItem setTitleView:view];
    
    
    self.secondaryImageView.center = view.center;
    self.logoImageView.center = view.center;
    
    
    [self showBackButtonIfNeeded:show];
}

- (void)setupNavigationBarWithTitleImage:(NSString *)imageName showBackButtonIfNeeded:(BOOL)show
{
    
    [self hideNavigationBar:NO];
    
    UIImageView *barImageView = [[UIImageView alloc] initWithImage:UIImageFromName(imageName)];
    
    [self.navigationItem setTitleView:barImageView];
    
    [self showBackButtonIfNeeded:show];
}

- (void)setupNavigationBarWithTitle:(NSString *)title showBackButtonIfNeeded:(BOOL)show
{
    [self hideNavigationBar:NO];
    
    [self showBackButtonIfNeeded:show];
    
    if (title != nil) self.navigationItem.title = title;
}

- (void)showNavBarSecondaryImageWithRatio:(float)ratio
{
    self.secondaryImageView.alpha = 1.0 - ratio;
    self.logoImageView.alpha = ratio;
}

- (void)hideRightNavigationButton
{
    self.rightNavigationButton.hidden = YES;
}

- (void)setRightNavigationButtonSelected:(BOOL)selected
{
    self.rightNavigationButton.selected = selected;
}

- (void)showRightNavigationButtonWithType:(UINavigationBarRightButtonType)type
{
    NSString *imageName;
    NSString *imageNameSelected;
    
    
    switch (type)
    {
        case UINavigationBarRightButtonTypeMenu:
            imageName = kRightNavigationButtonMenu;
            imageNameSelected = kRightNavigationButtonMenuSelected;
            break;
            
        case UINavigationBarRightButtonTypeAddNew:
            
            imageName = kRightNavigationButtonAddNew;
            imageNameSelected = kRightNavigationButtonAddNew;
            break;
            
        case UINavigationBarRightButtonTypeFilter:
            
            imageName = kRightNavigationButtonFilterCategories;
            imageNameSelected = kRightNavigationButtonFilteredCategories;
            break;
            
        case UINavigationBarRightButtonTypeLogout:
            
            imageName = kRightNavigationButtonLogout;
            imageNameSelected = kRightNavigationButtonLogout;
            break;
            
        default:
            
            imageName = kRightNavigationButtonAddNew;
            imageNameSelected = kRightNavigationButtonAddNew;
            break;
    }
    

    UIImage *optionsImage = UIImageFromName(imageName);
    UIImage *optionsImageSelected = UIImageFromName(imageNameSelected);
    
    if (!self.rightNavigationButton)// || [self.rightNavigationButton imageForState:UIControlStateNormal] != optionsImage)
    {
        [self createRightNavigationButtonWithImage:optionsImage selectedImage:optionsImageSelected];
    }
    
    self.rightNavigationButton.hidden = NO;
}

- (void)createRightNavigationButtonWithImage:(UIImage *)image selectedImage:(UIImage *)selectedImage
{
    self.rightNavigationButton = nil;

    
    self.rightNavigationButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, image.size.width, image.size.height)];
    
    [self.rightNavigationButton addTarget:self action:@selector(rightNavigationButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.rightNavigationButton setImage:image forState:UIControlStateNormal];
    [self.rightNavigationButton setImage:selectedImage forState:UIControlStateSelected];
    
    self.rightNavigationButton.clipsToBounds = NO;
    
    UIBarButtonItem *rightbtnItem = [[UIBarButtonItem alloc] initWithCustomView:self.rightNavigationButton];
    
    [[self navigationItem]setRightBarButtonItem:rightbtnItem];
}

- (void)showBackButtonIfNeeded:(BOOL)show
{
    if (show && self.navigationController.viewControllers.count > 1)
    {
        UIImage *backBarImage = UIImageFromName(kNavBackButton);
        
        self.leftNavButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, backBarImage.size.width, backBarImage.size.height)];
        
        [self.leftNavButton addTarget:self action:@selector(backButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        
        [self.leftNavButton setImage:backBarImage forState:UIControlStateNormal];
        
        UIBarButtonItem *backbtnItem = [[UIBarButtonItem alloc] initWithCustomView:self.leftNavButton];
        
        [[self navigationItem] setLeftBarButtonItem:backbtnItem];
    }
}

- (void)showRightNavigationMenuButton
{
    [self showRightNavigationButtonWithType:UINavigationBarRightButtonTypeMenu];
}

- (void)setBackButtonImage:(NSString *)imageName
{
    [self.leftNavButton setImage:UIImageFromName(imageName) forState:UIControlStateNormal];
}

- (void)hideNavigationBar:(BOOL)hide
{
    [self hideNavigationBar:hide animated:NO];
}

- (void)hideNavigationBar:(BOOL)hide animated:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:hide animated:animated];
}

- (void)enableEdgePanGestureRecognizerToPopViewController:(BOOL)enable
{
    if (enable)
    {
        self.edgePanGestureRecognizer = [[UIScreenEdgePanGestureRecognizer alloc] initWithTarget:self action:@selector(screenEdgePanGestureRecognized:)];
        [self.edgePanGestureRecognizer setEdges:UIRectEdgeLeft];
        self.edgePanGestureRecognizer.delegate = self;
        
        [self.view addGestureRecognizer:self.edgePanGestureRecognizer];
    }
}

- (void)enableTopSwipeGestureRecognizer:(BOOL)enable
{
    self.topSwipeGestureRecognizer.enabled = enable;
}

- (void)logoutToLoginViewController
{
    NSLog(@"Parent: %@", NSStringFromClass([self.parentViewController class]));
    
    
    [self.navigationController popToRootViewControllerAnimated:YES];

    [UtilityFunctions setValueInUserDefaults:nil forKey:@"user"];
}

#pragma mark - Right Navigation Button Animation

- (void)animateRightNavigationButtonWithDegree:(int)degree
{
    
    if(degree == 45)
    {
    
        CABasicAnimation *halfTurn;
        halfTurn = [CABasicAnimation animationWithKeyPath:@"transform.rotation"];
        halfTurn.fromValue = [NSNumber numberWithFloat:0];
        halfTurn.toValue = [NSNumber numberWithFloat:M_PI_2];
        halfTurn.duration = 0.2;
        halfTurn.removedOnCompletion = NO;
        halfTurn.repeatCount = 0;
        [self.rightNavigationButton.layer addAnimation:halfTurn forKey:@"rotation"];
        
    
    }

}


#pragma mark - UISwipeGestureRecognizer Method

- (UISwipeGestureRecognizer *)topSwipeGestureRecognizer
{
    if (!_topSwipeGestureRecognizer)
    {
        _topSwipeGestureRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(topSwipeGestureRecognized:)];
        _topSwipeGestureRecognizer.direction = UISwipeGestureRecognizerDirectionDown;
        
        [self.navigationController.navigationBar addGestureRecognizer:_topSwipeGestureRecognizer];
    }
    
    return _topSwipeGestureRecognizer;
}

- (void)topSwipeGestureRecognized:(UISwipeGestureRecognizer *)sender
{
    NSLog(@"top Swipe Gesture Recognized");
    
    if (self.baseDelegate && [self.baseDelegate respondsToSelector:@selector(topSwipeDownGestureRecognized)])
    {
        [self.baseDelegate topSwipeDownGestureRecognized];
    }
}




#pragma mark - UIScreenEdgePanGestureRecognizer Method

- (void)screenEdgePanGestureRecognized:(UIScreenEdgePanGestureRecognizer *)sender
{
    if (sender.state == UIGestureRecognizerStateBegan)
    {
        NSLog(@"screenEdgePanGestureRecognized");
        if (self.navigationController.viewControllers.count > 1)
        {
            [self.navigationController popViewControllerAnimated:YES];
        }
    }
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    return YES;
}




#pragma mark - Buttons Actions

- (void)rightNavigationButtonClicked:(id)sender
{
    NSLog(@"Right Navigation Button Clicked");
    
    if (self.baseDelegate && [self.baseDelegate respondsToSelector:@selector(rightNavigationButton)])
    {
        [self.baseDelegate rightNavigationBarButtonClicked];
    }
    
    if ([self.parentViewController.parentViewController isKindOfClass:[MenuViewController class]])
    {
        [(MenuViewController *)self.parentViewController.parentViewController hideMenuAnimated:YES];
        self.rightNavigationButton.selected = !self.rightNavigationButton.selected;
    }
}

- (void)backButtonClicked:(id)sender
{
    NSLog(@"Left Navigation Button Clicked");
    
    if (self.baseDelegate && [self.baseDelegate respondsToSelector:@selector(leftNavigationBarButtonClicked)])
    {
        [self.baseDelegate leftNavigationBarButtonClicked];
    }

    [self.navigationController popViewControllerAnimated:YES];
}

@end
