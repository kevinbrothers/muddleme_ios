//
//  HomeTableViewCell.m
//  Muddleme
//
//  Created by Asad Ali on 09/08/2015.
//  Copyright (c) 2015 AESquares. All rights reserved.
//

#import "HomeTableViewCell.h"
#import "nuHelpers.h"


@implementation HomeTableViewCell


- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    self.imgBg.image = UIImageFromName(selected? @"home_cell_selected_bg":@"home_cell_normal_bg");
}

@end
