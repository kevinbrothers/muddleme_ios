//
//  HomeViewController.h
//  Muddleme
//
//  Created by Asad Ali on 09/08/2015.
//  Copyright (c) 2015 AESquares. All rights reserved.
//

#import "BaseViewController.h"

@interface HomeViewController : BaseViewController{
    int selectedIndex;
    BOOL longPressed;
}
@property (weak, nonatomic) IBOutlet UIView *joinView;
@property (weak, nonatomic) IBOutlet UILabel *loyaltyNamelbl;


@end
