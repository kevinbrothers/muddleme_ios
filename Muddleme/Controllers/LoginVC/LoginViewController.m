//
//  LoginViewController.m
//  Muddleme
//
//  Created by Asad Ali on 09/08/2015.
//  Copyright (c) 2015 AESquares. All rights reserved.
//

#import "LoginViewController.h"
#import "MenuViewController.h"


@interface LoginViewController ()


@property (weak, nonatomic) IBOutlet UITextField *txtEmail;
@property (weak, nonatomic) IBOutlet UITextField *txtPassword;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *txtPasswordTopConstrain;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *txtEmailTopConstrain;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *btnLoginBottomConstrain;
@property (weak, nonatomic) IBOutlet UIImageView *imgEmailValidator;
@property (weak, nonatomic) IBOutlet UIImageView *imgPasswordValidator;


@property (assign, nonatomic) BOOL isViewDidLayoutSubviews;


@end


@implementation LoginViewController



#pragma mark - UIViewController Life Cycle

- (void)viewDidLoad
{
    [super viewDidLoad];

    [self setupNavigationBarWithTitle:@"Login" showBackButtonIfNeeded:YES];
    
    self.txtEmail.attributedPlaceholder = PlaceHolderAttributedString(@"Enter your email");
    self.txtPassword.attributedPlaceholder = PlaceHolderAttributedString(@"Enter your password");
    
    self.imgEmailValidator.hidden = YES;
    self.imgPasswordValidator.hidden = YES;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidLayoutSubviews
{
    if (!self.isViewDidLayoutSubviews)
    {
        if ([SizeClass currentDeviceSizes].isDeviceIPhone5 || [SizeClass currentDeviceSizes].isDeviceIPhone4)
        {
            self.txtEmailTopConstrain.constant = 0.0;
            self.txtPasswordTopConstrain.constant = 10.0;
            self.btnLoginBottomConstrain.constant = 20.0;
        }
    }
    
    self.isViewDidLayoutSubviews = YES;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}



#pragma mark - Buttons Action

- (IBAction)btnLoginClicked:(id)sender
{
    [self.navigationController pushViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"MenuViewController"] animated:YES];
    
    if ([self isFormValid])
    {
        [SVProgressHUD show];
        
        NSDictionary *params = @{@"user":@{@"email":self.txtEmail.text, @"password":self.txtPassword.text}};
        
        [[SVHTTPClient sharedClient] POST:@"login.json" parameters:params completion:^(id response, NSHTTPURLResponse *urlResponse, NSError *error)
        {
            [SVProgressHUD dismiss];
            
            NSLog(@"response: %@", response);
            
            if ([response isKindOfClass:[NSDictionary class]])
            {
                [self.navigationController pushViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"MenuViewController"] animated:YES];
                
                NSDictionary *userInfo = @{@"email":response[@"email"], @"id":response[@"id"], @"first_name":[NSString stringWithFormat:@"%@ %@", response[@"first_name"], response[@"last_name"]]};
                
                [UtilityFunctions setValueInUserDefaults:userInfo forKey:@"user"];
            }
            else
            {
                [self showAlertViewWithTitle:kEmptyString message:@"Email address or Password is incorrect"];
            }
        }];
    }
}



#pragma mark - UITextField Delegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField == self.txtEmail)
    {
        [self.txtPassword becomeFirstResponder];
    }
    else
    {
        [textField resignFirstResponder];
        
        // Call Signin API
        [self btnLoginClicked:nil];
    }
    
    return YES;
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:YES];
}



#pragma mark - Validations

- (BOOL)isFormValid
{
    [self showFormValidationImages];
    
    
    if (![UtilityFunctions isValidEmailAddress:self.txtEmail.text])
    {
        [self showAlertViewWithTitle:kEmptyString message:@"Please enter valid email address" withDismissCompletion:^{
            
            [self.txtEmail becomeFirstResponder];
        }];
        
        return NO;
    }
    else if (self.txtPassword.text.length <= 3)
    {
        [self showAlertViewWithTitle:kEmptyString message:@"Please enter valid password" withDismissCompletion:^{
            
            [self.txtPassword becomeFirstResponder];
        }];
        
        return NO;
    }
    
    
    return YES;
}


- (void)showFormValidationImages
{
    self.imgEmailValidator.hidden = NO;
    self.imgPasswordValidator.hidden = NO;
    
    self.imgEmailValidator.image = UIImageFromName(@"correct_icon");
    self.imgPasswordValidator.image = UIImageFromName(@"correct_icon");
    
    
    if (![UtilityFunctions isValidEmailAddress:self.txtEmail.text])
    {
        self.imgEmailValidator.image = UIImageFromName(@"wrong_icon");
    }
    if (self.txtPassword.text.length <= 3)
    {
        self.imgPasswordValidator.image = UIImageFromName(@"wrong_icon");
    }
}


@end
