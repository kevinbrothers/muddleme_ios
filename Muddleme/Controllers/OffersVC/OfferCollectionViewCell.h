//
//  OfferCollectionViewCell.h
//  Muddleme
//
//  Created by Asad Ali on 16/09/2015.
//  Copyright (c) 2015 AESquares. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Muddleme-Swift.h"


@interface OfferCollectionViewCell : UICollectionViewCell


@property (strong, nonatomic) Coupon *cellData;

@end
