//
//  AuctionFeedbackViewController.m
//  Muddleme
//
//  Created by Asad Ali on 09/08/2015.
//  Copyright (c) 2015 AESquares. All rights reserved.
//

#import "AuctionFeedbackViewController.h"
#import "MyAccountTableViewCell.h"

@interface AuctionFeedbackViewController ()
@property (weak, nonatomic) IBOutlet UIView *containerView;
@property (weak, nonatomic) IBOutlet UITableView *tblAccountDetails;
@property (weak, nonatomic) IBOutlet UITextView *textView;
@property (strong, nonatomic) NSMutableArray *collectionItems;

@end

@implementation AuctionFeedbackViewController



#pragma mark - UIViewController Life Cycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    
    [self setupNavigationBarWithMenuForTitle:@"Merchant Loyality Feedback"];
    [self.containerView roundTheCornersWithBorder:4.0];
    
    self.textView.layer.cornerRadius = 10;
    self.textView.layer.borderWidth = 2;
    self.textView.layer.borderColor = [UIColor whiteColor].CGColor;
    
    if (self.currentProgram)
    {
        self.NameLbl.text = self.currentProgram.Name;
        
        [self getAllReviews];
    }
    
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}
- (void)getAllReviews
{
    
    
    
    NSMutableDictionary* params = [[NSMutableDictionary alloc] init];
    
    //[params setObject:dict[@"id"] forKey:@"user_id"];
    
    [SVProgressHUD show];
    
    NSString* url = [NSString stringWithFormat:@"loyalty_programs/%lu/reviews",self.currentProgram.PId];

    
    
    [[UtilityFunctions authenticatedHTTPClient] GET:url parameters:params completion:^(id response, NSHTTPURLResponse *urlResponse, NSError *error)
     {
         [SVProgressHUD dismiss];
         
         if (!error)
         {
             
             self.collectionItems = response;
//             self.joinedItems = [[NSMutableArray alloc ]init];
//             
//             
//             for (NSDictionary* dict in response)  {
//                 
//                 LoyaltyProgram* programm = [[LoyaltyProgram alloc] init];
//                 [programm InintWithDic:dict];
//                 [self.joinedItems addObject:programm];
//                 
//                 for (LoyaltyProgram* pgm in self.collectionItems)
//                 {
//                     if(pgm.PId == programm.PId)
//                     {
//                         pgm.bIsJoined = YES;
//                         break;
//                     }
//                 }
//             }
             
             
             [self.tblAccountDetails reloadData];
         }
         else
         {
             [self showAlertViewWithTitle:kEmptyString message:error.localizedDescription];
         }
     }];
}




#pragma mark - Buttons Action

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.collectionItems.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"MyAccountTableViewCell";
    
    MyAccountTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    
    [cell removeCellSeparatorOffset];
    
    NSDictionary* dic = [self.collectionItems objectAtIndex:indexPath.row];
    
    cell.lblTitle.text = dic[@"title"];
    cell.lblValue.text = dic[@"comment"];

    return cell;
}

-(IBAction)AddRewview:(id)sender{
    
 
    
    
    NSMutableDictionary* params = [[NSMutableDictionary alloc] init];
    
    
    [params setObject:self.textView.text forKey:@"review"];

    
    
    NSString* url = [NSString stringWithFormat:@"loyalty_programs/%li.json",self.currentProgram.PId];
    
    
    
    [SVProgressHUD show];
    
    [[UtilityFunctions authenticatedHTTPClient] POST:url parameters:params completion:^(id response, NSHTTPURLResponse *urlResponse, NSError *error)
     {
         [SVProgressHUD dismiss];
         
         if (!error)
         {
             //[self.collectionItems addObject:response];;
             //itemDict.bIsJoined = true;
             
             //[self.tblAccountDetails reloadData];
             
             [self getAllReviews];
         }
         else
         {
             [self showAlertViewWithTitle:kEmptyString message:error.localizedDescription];
         }
     }];
    
}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
}


- (BOOL)textViewShouldBeginEditing:(UITextView *)textView_
{
    
    if ([textView_.text isEqualToString:@"Start typing here .."])
    {
        textView_.text=@"";
        //textView_.textColor=[UIColor blackColor];
    }
    
    return YES;
}

//- (void)textViewDidBeginEditing:(UITextView *)textView{
//    textView.text=@"";
//}
- (void)textViewDidEndEditing:(UITextView *)textView_
{
    [textView_ resignFirstResponder];
    if ([textView_.text isEqualToString:@""])
    {
        textView_.text=@"Start typing here ..";
       // textView_.textColor=[UIColor grayColor];
    }
    else
    {
       // textView_.textColor=[UIColor blackColor];
    }
    
}
@end
