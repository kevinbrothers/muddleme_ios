//
//  SignupViewController.m
//  Muddleme
//
//  Created by Asad Ali on 09/08/2015.
//  Copyright (c) 2015 AESquares. All rights reserved.
//

#import "SignupViewController.h"
#import "MenuViewController.h"

@interface SignupViewController () <UITextFieldDelegate>


@property (weak, nonatomic) IBOutlet UIView *containerView;
@property (weak, nonatomic) IBOutlet UITextField *txtEmail;
@property (weak, nonatomic) IBOutlet UITextField *txtPassword;
@property (weak, nonatomic) IBOutlet UITextField *txtName;
@property (weak, nonatomic) IBOutlet UITextField *txtMobileNumber;
@property (weak, nonatomic) IBOutlet UIButton *btnCheckbox;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *txtNameTopConstrain;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *txtPasswordTopConstrain;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *txtEmailTopConstrain;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *txtPhoneTopConstrain;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *btnCheckboxTopConstrain;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *btnSignupTopConstrain;

@property (weak, nonatomic) IBOutlet UIImageView *imgNameValidator;
@property (weak, nonatomic) IBOutlet UIImageView *imgEmailValidator;
@property (weak, nonatomic) IBOutlet UIImageView *imgPasswordValidator;
@property (weak, nonatomic) IBOutlet UIImageView *imgTelephoneValidator;


@property (assign, nonatomic) BOOL isViewDidLayoutSubviews;


@end


@implementation SignupViewController



#pragma mark - UIViewController Life Cycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.imgEmailValidator.hidden = YES;
    self.imgPasswordValidator.hidden = YES;
    self.imgNameValidator.hidden = YES;
    self.imgTelephoneValidator.hidden = YES;
    
    self.txtEmail.attributedPlaceholder = PlaceHolderAttributedString(self.txtEmail.placeholder);
    self.txtName.attributedPlaceholder = PlaceHolderAttributedString(self.txtName.placeholder);
    self.txtPassword.attributedPlaceholder = PlaceHolderAttributedString(self.txtPassword.placeholder);
    self.txtMobileNumber.attributedPlaceholder = PlaceHolderAttributedString(self.txtMobileNumber.placeholder);

    [self setupNavigationBarWithTitle:@"signup" showBackButtonIfNeeded:YES];
    
    [self btnCheckboxClicked:self.btnCheckbox];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidLayoutSubviews
{
    if (!self.isViewDidLayoutSubviews)
    {
        if ([SizeClass currentDeviceSizes].isDeviceIPhone5 || [SizeClass currentDeviceSizes].isDeviceIPhone4)
        {
            self.txtEmailTopConstrain.constant = 5.0;
            self.txtPasswordTopConstrain.constant = 5.0;
            self.txtPhoneTopConstrain.constant = 5.0;
            self.btnCheckboxTopConstrain.constant = 5.0;
            self.btnSignupTopConstrain.constant = 5.0;
        }
    }
    
    self.isViewDidLayoutSubviews = YES;
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}



#pragma mark - Buttons Action

- (IBAction)btnSignupClicked:(id)sender
{
    if ([self isFormValid])
    {
        [SVProgressHUD show];
        
        NSDictionary *params = @{@"user":@{@"email":self.txtEmail.text, @"password":self.txtPassword.text, @"first_name":self.txtName.text, @"phone":self.txtMobileNumber.text}};
        
        [[SVHTTPClient sharedClient] POST:@"user.json" parameters:params completion:^(id response, NSHTTPURLResponse *urlResponse, NSError *error)
         {
            [SVProgressHUD dismiss];
             
             if ([response isKindOfClass:[NSDictionary class]])
             {
                 NSString *error = [response objectForKeyNotNull:@"error"];
                 
                 if (error && error.length > 0)
                 {
                     [self showAlertViewWithTitle:kEmptyString message:error];
                 }
                 else
                 {
                     User * user = [User sharedInstance];
                     [user InintWithDic:response];
                     
                     
                     
                     NSDictionary *userInfo = @{@"email":response[@"email"], @"id":response[@"id"],@"access_token":response[@"access_token"], @"first_name":[NSString stringWithFormat:@"%@ %@", response[@"first_name"], response[@"last_name"]], @"balance":response[@"balance"], @"total_balance":response[@"total_balance"], @"pending_outcome":response[@"pending_outcome"]};
                     
                     [UtilityFunctions setValueInUserDefaults:userInfo forKey:@"user"];
                     [self.navigationController pushViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"MenuViewController"] animated:YES];
                 }
             }else if([response isKindOfClass:[NSArray class]])
             {
                 [self showAlertViewWithTitle:kEmptyString message:response[0]];
             }
             else
             {
                 [self showAlertViewWithTitle:kEmptyString message:@"Something went wrong. Please try later."];
             }
         }];
    }
}

- (IBAction)btnCheckboxClicked:(id)sender
{
    self.btnCheckbox.selected = !self.btnCheckbox.selected;
}

- (IBAction)btnTermsOfServiceClicked:(id)sender
{
}



#pragma mark - UITextField Delegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField == self.txtName)
    {
        [self.txtPassword becomeFirstResponder];
    }
    else if (textField == self.txtPassword)
    {
        [self.txtEmail becomeFirstResponder];
    }
    else if (textField == self.txtEmail)
    {
        [self.txtMobileNumber becomeFirstResponder];
    }
    else
    {
        [textField resignFirstResponder];
        
        // Call Signup API
        [self btnSignupClicked:nil];
    }
    
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    BOOL isSmallDevice = [SizeClass currentDeviceSizes].isDeviceIPhone4 || [SizeClass currentDeviceSizes].isDeviceIPhone5;
    
    if (textField == self.txtName)
    {
        self.txtNameTopConstrain.constant = 0.0;
    }
    else if (textField == self.txtPassword)
    {
        self.txtNameTopConstrain.constant = isSmallDevice? -55.0:-70.0;
    }
    else if (textField == self.txtEmail)
    {
        self.txtNameTopConstrain.constant = isSmallDevice? -110.0:-140.0;
    }
    else
    {
        self.txtNameTopConstrain.constant = isSmallDevice? -165.0:-210.0;
    }
    
    [UIView animateWithDuration:kAnimationDuration animations:^{ [self.view layoutIfNeeded]; }];
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    self.txtNameTopConstrain.constant = 0.0;
    
    [UIView animateWithDuration:kAnimationDuration animations:^{ [self.view layoutIfNeeded]; }];
    return YES;
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:YES];
}




#pragma mark - Validations

- (BOOL)isFormValid
{
    [self showFormValidationImages];
    
    
    if (self.txtName.text.cleanedString.length == 0)
    {
        [self showAlertViewWithTitle:kEmptyString message:@"Please enter your name" withDismissCompletion:^{
            
            [self.txtName becomeFirstResponder];
        }];
        
        return NO;
    }
    else if (self.txtPassword.text.length <= 3)
    {
        [self showAlertViewWithTitle:kEmptyString message:@"Please enter valid password" withDismissCompletion:^{
            
            [self.txtPassword becomeFirstResponder];
        }];
        
        return NO;
    }
    else if (![UtilityFunctions isValidEmailAddress:self.txtEmail.text])
    {
        [self showAlertViewWithTitle:kEmptyString message:@"Please enter valid email address" withDismissCompletion:^{
            
            [self.txtEmail becomeFirstResponder];
        }];
        
        return NO;
    }
    else if (self.txtMobileNumber.text.length <= 4)
    {
        [self showAlertViewWithTitle:kEmptyString message:@"Please enter valid phone number" withDismissCompletion:^{
            
            [self.txtMobileNumber becomeFirstResponder];
        }];
        
        return NO;
    }
    else if (!self.btnCheckbox.selected)
    {
        [self showAlertViewWithTitle:kEmptyString message:@"Please agree to our Terms of Service first"];
        
        return NO;
    }
    
    return YES;
}

- (void)showFormValidationImages
{
    self.imgEmailValidator.hidden = NO;
    self.imgPasswordValidator.hidden = NO;
    self.imgNameValidator.hidden = NO;
    self.imgTelephoneValidator.hidden = NO;
    
    self.imgEmailValidator.image = UIImageFromName(@"correct_icon");
    self.imgPasswordValidator.image = UIImageFromName(@"correct_icon");
    self.imgTelephoneValidator.image = UIImageFromName(@"correct_icon");
    self.imgNameValidator.image = UIImageFromName(@"correct_icon");
    
    
    if (![UtilityFunctions isValidEmailAddress:self.txtEmail.text])
    {
        self.imgEmailValidator.image = UIImageFromName(@"wrong_icon");
    }
    if (self.txtPassword.text.length <= 3)
    {
        self.imgPasswordValidator.image = UIImageFromName(@"wrong_icon");
    }
    if (self.txtName.text.cleanedString.length == 0)
    {
        self.imgNameValidator.image = UIImageFromName(@"wrong_icon");
    }
    if (self.txtMobileNumber.text.length <= 4)
    {
        self.imgTelephoneValidator.image = UIImageFromName(@"wrong_icon");
    }
    
}





@end

