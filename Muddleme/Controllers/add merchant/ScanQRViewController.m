//
//  ScanQRViewController.m
//  Muddleme
//
//  Created by Arkhitech on 14/12/2015.
//  Copyright © 2015 AESquares. All rights reserved.
//

#import "ScanQRViewController.h"

@interface ScanQRViewController ()
@property (weak, nonatomic) IBOutlet UIView *containerView;
@property (weak, nonatomic) IBOutlet UIView *previewView;

@end

@implementation ScanQRViewController

#pragma mark - UIViewController Life Cycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    
    [self setupNavigationBarWithTitle:@"scan qr code" showBackButtonIfNeeded:YES];
    
    scanner = [[MTBBarcodeScanner alloc] initWithPreviewView:self.previewView];
    
//    [MTBBarcodeScanner requestCameraPermissionWithSuccess:^(BOOL success) {
//        if (success) {
//            
//            [scanner startScanningWithResultBlock:^(NSArray *codes) {
//                AVMetadataMachineReadableCodeObject *code = [codes firstObject];
//                NSLog(@"Found code: %@", code.stringValue);
//                
//                [scanner stopScanning];
//            }];
//            
//        } else {
//            // The user denied access to the camera
//        }
//    }];
    
    
    [scanner startScanningWithResultBlock:^(NSArray *codes) {
        for (AVMetadataMachineReadableCodeObject *code in codes) {
            NSLog(@"Found code: %@", code.stringValue);
            
            [scanner captureStillImage:^(UIImage *image, NSError *error) {
                
                [self.delegate ImageCaptured:image];
                [self.navigationController popViewControllerAnimated:YES];
            }];
        }
        [scanner stopScanning];
    }];
  
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


-(IBAction)Capture:(id)sender
{
    [scanner stopScanning];

    [scanner captureStillImage:^(UIImage *image, NSError *error) {
        
        [self.delegate ImageCaptured:image];
        [self.navigationController popViewControllerAnimated:YES];
    }];
}


#pragma mark - Buttons Action

@end
