//
//  NSArray+Utilities.h
//  Muddleme
//
//  Created by Asad Ali on 29/03/2015.
//  Copyright (c) 2015 AESquares. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSArray (Utilities)


@property (nonatomic, readonly) NSString *jsonString;


- (void)splitDataForTwoCollectionViews:(void (^)(NSArray *leftCollectionViewArray, NSArray *rightCollectionViewArray))completionBlock;



@end
