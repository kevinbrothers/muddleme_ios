//
//  NSArray+Utilities.m
//  Muddleme
//
//  Created by Asad Ali on 29/03/2015.
//  Copyright (c) 2015 AESquares. All rights reserved.
//

#import "NSArray+Utilities.h"
#import "NSDictionary+Utilities.h"

@implementation NSArray (Utilities)



- (void)splitDataForTwoCollectionViews:(void (^)(NSArray *leftCollectionViewArray, NSArray *rightCollectionViewArray))completionBlock
{
    NSMutableArray *forLeft = [NSMutableArray new];
    NSMutableArray *forRight = [NSMutableArray new];
    
    if (self.count > 0)
    {
        for (int i = 0; i < self.count; i ++)
        {
            if (i % 2 == 0)
            {
                [forLeft addObject:self[i]];
            }
            else
            {
                [forRight addObject:self[i]];
            }
        }
    }
    
    completionBlock(forLeft, forRight);
}




- (NSString *)jsonString
{
    NSError *error = nil;
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:self options:0 error:&error];
    
    return [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
}


@end
