//
//  NSDictionary+Utilities.h
//  ConsumerBreak
//
//  Created by My Mac on 17/11/2014.
//  Copyright (c) 2014 Zeeshan Haider. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface NSDictionary (Utilities)



#pragma mark - Muddleme Data Helper


- (BOOL)isProductHasCampaigns;

- (BOOL)isProductInAppPurchasable;

- (BOOL)isDictionaryContainsBrandData;


- (NSString *)brandStoreAddress;





#pragma mark - Utilities


- (id)objectForKeyNotNull:(id)key;
- (id)objectForKeyWithEmptyString:(id)key;
- (BOOL)isKindOfClass:(Class)aClass forKey:(NSString *)key;
- (BOOL)isMemberOfClass:(Class)aClass forKey:(NSString *)key;
- (BOOL)isArrayForKey:(NSString *)key;
- (BOOL)isDictionaryForKey:(NSString *)key;
- (BOOL)isStringForKey:(NSString *)key;
- (BOOL)isNumberForKey:(NSString *)key;

- (NSArray *)arrayForKey:(NSString *)key;
- (NSDictionary *)dictionaryForKey:(NSString *)key;
- (NSString *)stringForKey:(NSString *)key;
- (NSNumber *)numberForKey:(NSString *)key;
- (double)doubleForKey:(NSString *)key;
- (float)floatForKey:(NSString *)key;
- (int)intForKey:(NSString *)key;
- (unsigned int)unsignedIntForKey:(NSString *)key;
- (NSInteger)integerForKey:(NSString *)key;
- (NSUInteger)unsignedIntegerForKey:(NSString *)key;
- (long long)longLongForKey:(NSString *)key;
- (unsigned long long)unsignedLongLongForKey:(NSString *)key;
- (BOOL)boolForKey:(NSString *)key;
//Extensions
- (bool)existsValue:(NSString*)expectedValue forKey:(NSString*)key;
- (NSInteger)integerValueForKey:(NSString*)key defaultValue:(NSInteger)defaultValue withRange:(NSRange)range;
- (NSInteger)integerValueForKey:(NSString*)key defaultValue:(NSInteger)defaultValue;
- (BOOL)typeValueForKey:(NSString*)key isArray:(BOOL*)bArray isNull:(BOOL*)bNull isNumber:(BOOL*)bNumber isString:(BOOL*)bString;
- (BOOL)valueForKeyIsArray:(NSString*)key;
- (BOOL)valueForKeyIsNull:(NSString*)key;
- (BOOL)valueForKeyIsString:(NSString*)key;
- (BOOL)valueForKeyIsNumber:(NSString*)key;
- (BOOL)containsKey: (NSString *)key;

- (NSDictionary*)dictionaryWithLowercaseKeys;
+(NSDictionary*)dictionaryWithContentsOfJSONString:(NSString*)fileLocation;
@end
