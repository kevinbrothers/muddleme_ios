//
//  NSString+Helper.m
//  Muddleme
//
//  Created by Asad Ali on 25/03/2015.
//  Copyright (c) 2015 AESquares. All rights reserved.
//

#import "NSString+Helper.h"
#import "AppConstants.h"
#import "UIMacros.h"
#include "AppConstants.h"

@implementation NSString (Helper)


- (NSString *)standardDateString
{
    return [self stringByReplacingOccurrencesOfString:@"T" withString:@" "];
}

- (NSString *)cleanedString
{
    return [self removeLeadingAndTrailingSpaces];
}


- (NSURL *)URL
{
    return [NSURL URLWithString:self];
}

- (CGSize)textSizeWithFont:(UIFont *)font
{
    return [self textSizeForWidth:CGFLOAT_MAX withFont:font];
}

- (CGSize)textSizeForWidth:(CGFloat)width withFont:(UIFont *)font
{
    CGSize maximumLabelSize = CGSizeMake(width, CGFLOAT_MAX);
    
    CGRect textRect = [self boundingRectWithSize:maximumLabelSize
                                         options:(NSStringDrawingUsesLineFragmentOrigin)
                                      attributes:@{NSFontAttributeName:font}
                                         context:nil];
    
    return textRect.size;
}


- (CGSize)sizeForHomeCellDescriptionTextForWidth:(CGFloat)width
{
    return [self textSizeForWidth:width withFont:FontLight(9.0)];
}


- (id)ParseJSONString
{
    NSError *error = nil;
    
    id jsonOjb = [NSJSONSerialization JSONObjectWithData:[self dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingAllowFragments error:&error];
    
    if (error) NSLog(@"NSJSONSerialization Error: %@", error);
    
    return jsonOjb;
}


- (NSAttributedString *)underlineAttributedStringForFont:(UIFont *)font textColor:(UIColor *)color
{
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:self];
    
    
    [attributedString addAttributes:@{NSUnderlineStyleAttributeName:@(NSUnderlineStyleSingle), NSFontAttributeName:font, NSForegroundColorAttributeName:color} range:NSMakeRange (0, self.length)];
    
    
    return attributedString;
}


- (NSAttributedString *)stringWithIncreasedLineSpacingForFont:(UIFont *)font textColor:(UIColor *)color
{
    NSMutableAttributedString* attrString = [[NSMutableAttributedString alloc] initWithString:self];
    
    NSMutableParagraphStyle *style = [[NSMutableParagraphStyle alloc] init];
    
    [style setLineSpacing:3];
    
    
    [attrString addAttributes:@{NSParagraphStyleAttributeName:style, NSFontAttributeName:font, NSForegroundColorAttributeName:color} range:NSMakeRange(0, self.length)];
    
    return attrString;
}


- (NSAttributedString *)descriptionTextStringWithIncreasedLineSpacing
{
    return [self stringWithIncreasedLineSpacingForFont:FontLight(10.0) textColor:ColorBlack];
}



- (NSString *)stringByRemovingCurrencyString
{
    return [self stringByReplacingOccurrencesOfString:kBaseCurrency withString:@""];
}


- (NSString *)stringByRemovingFirstAndLastCharacter
{
    return [[self stringByReplacingCharactersInRange:NSMakeRange(0, 1) withString:@""] stringByReplacingCharactersInRange:NSMakeRange(self.length -2, 1) withString:@""];
}


- (NSString *)removeLeadingAndTrailingSpaces
{
    return [self stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
}


- (NSString *)maskedCardNumberString
{
    return [self stringByReplacingCharactersInRange:NSMakeRange(0, self.length-4) withString:@"*"];
}


@end







@implementation NSAttributedString (Helper)


- (NSAttributedString *)underlineAttributedString
{
    NSMutableAttributedString *attributedString = [self mutableCopy];
    
    
    [attributedString addAttributes:@{NSUnderlineStyleAttributeName:@(NSUnderlineStyleSingle)} range:NSMakeRange (0, self.length)];
    
    
    return attributedString;
}


- (CGSize)textSizeForWidth:(CGFloat)width
{
    CGSize maximumLabelSize = CGSizeMake(width, CGFLOAT_MAX);
    
    CGRect textRect = [self boundingRectWithSize:maximumLabelSize options:NSStringDrawingUsesLineFragmentOrigin context:nil];
    
    return textRect.size;
}



@end
