//
//  UITableViewCell+Helper.m
//  Muddleme
//
//  Created by Asad Ali on 26/05/2015.
//  Copyright (c) 2015 AESquares. All rights reserved.
//

#import "UITableViewCell+Helper.h"

@implementation UITableViewCell (Helper)


- (void)removeCellSeparatorOffset
{
    if ([self respondsToSelector:@selector(setSeparatorInset:)])
        [self setSeparatorInset:UIEdgeInsetsZero];
    
    if ([self respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)])
        [self setPreservesSuperviewLayoutMargins:NO];
    
    
    if ([self respondsToSelector:@selector(setLayoutMargins:)])
        [self setLayoutMargins:UIEdgeInsetsZero];
}


@end
