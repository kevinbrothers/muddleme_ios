//
//  UIView+Helper.h
//  Muddleme
//
//  Created by Asad Ali on 24/03/2015.
//  Copyright (c) 2015 AESquares. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (Helper)



@property (readonly, nonatomic) CGFloat width;
@property (readonly, nonatomic) CGFloat height;
@property (readonly, nonatomic) CGFloat xPosition;
@property (readonly, nonatomic) CGFloat yPosition;



- (void)addUpperBorder;

- (void)addBorderWithWidth:(CGFloat)borderWidth borderColor:(UIColor *)borderColor;

- (void)removeBorder;

- (void)roundTheCorners:(CGFloat)radius;

- (void)addShadow:(CGFloat)radius;

- (void)roundTheCornersWithBorder:(CGFloat)radius;

- (void)roundedView;

- (void)roundedViewWithBorderWidth:(CGFloat)width borderColor:(UIColor *)color;

- (void)setRoundedCorners:(UIRectCorner)corners radius:(float)radius;// viewSize:(CGSize)size;

- (void)createHalfCircleView:(UIView *)view;



- (void)removeAllSubviews;


@end
