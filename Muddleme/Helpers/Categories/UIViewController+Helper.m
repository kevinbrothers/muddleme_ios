//
//  UIViewController+Helper.m
//  Muddleme
//
//  Created by Asad Ali on 25/03/2015.
//  Copyright (c) 2015 AESquares. All rights reserved.
//

#import "UIViewController+Helper.h"

@implementation UIViewController (Helper)



- (void)addChildViewControllerInContainer:(UIView *)containerView childViewController:(UIViewController *)controller
{
    [self removeAllChildViewControllers];
    
    [self addChildViewController:controller];
    controller.view.frame = containerView.bounds;
    [containerView insertSubview:controller.view atIndex:0];
    [controller didMoveToParentViewController:self];
}

- (void)addChildViewControllerInContainer:(UIView *)containerView childViewController:(UIViewController *)controller preserverViewController:(UIViewController *)dontDeleteVC
{
    [self removeAllChildViewControllersExcept:dontDeleteVC];
    
    [self addChildViewController:controller];
    controller.view.frame = containerView.bounds;
    [containerView insertSubview:controller.view atIndex:0];
    [controller didMoveToParentViewController:self];
}


- (void)removeAllChildViewControllers
{
    for (UIViewController *childController in self.childViewControllers)
    {
        [childController willMoveToParentViewController:nil];
        
        [childController.view removeFromSuperview];
        
        [childController removeFromParentViewController];
    }
}


- (void)removeAllChildViewControllersExcept:(UIViewController *)vc
{
    for (UIViewController *childController in self.childViewControllers)
    {
        if (childController != vc)
        {
            [childController willMoveToParentViewController:nil];
            
            [childController.view removeFromSuperview];
            
            [childController removeFromParentViewController];
        }
    }
}




#pragma mark - UIAlertController Methods

- (void)showAlertViewWithTitle:(NSString *)title message:(NSString *)message
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    
    [alert addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleCancel handler:nil]];
    
    [self presentViewController:alert animated:YES completion:^{}];
}


- (void)showAlertViewWithTitle:(NSString *)title message:(NSString *)message withDismissCompletion:(AlertViewDismissHandler)dismissHandler
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    
    [alert addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        dismissHandler();
    }]];
    
    [self presentViewController:alert animated:YES completion:^{}];
}


- (void)showConfirmationAlertViewWithTitle:(NSString *)title message:(NSString *)message withDismissCompletion:(AlertViewConfirmedButtonClickedHandler)dismissHandler
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    
    [alert addAction:[UIAlertAction actionWithTitle:@"YES" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        dismissHandler();
    }]];
    
    [alert addAction:[UIAlertAction actionWithTitle:@"NO" style:UIAlertActionStyleCancel handler:nil]];
    
    
    [self presentViewController:alert animated:YES completion:^{}];
}


- (void)showCurrentPasswordConfirmationAlertViewWithTitle:(NSString *)title message:(NSString *)message withDismissCompletion:(AlertViewCurrentPasswordConfirmedHandler)dismissHandler
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    
    
    [alert addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        dismissHandler(@"");
    }]];

    
    UIAlertAction *confirmAction = [UIAlertAction actionWithTitle:@"Update Password" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        UITextField *txtCurrentPassword = alert.textFields.lastObject;
        dismissHandler(txtCurrentPassword.text);
    }];
    

    [alert addAction:confirmAction];
    
    
    
    [alert addTextFieldWithConfigurationHandler:^(UITextField *textField)
     {
         textField.placeholder = @"Current Password";
         textField.secureTextEntry = YES;
         
         [textField addTarget:self action:@selector(alertTextFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
     }];
    
    
    [self presentViewController:alert animated:YES completion:^{}];
}

- (void)showPINConfirmationAlertViewWithDismissCompletion:(AlertViewCurrentPasswordConfirmedHandler)dismissHandler
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Muddleme PIN" message:@"Please enter your App PIN" preferredStyle:UIAlertControllerStyleAlert];
    
    
    [alert addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {}]];
    
    
    UIAlertAction *confirmAction = [UIAlertAction actionWithTitle:@"Confirm" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        UITextField *txtCurrentPassword = alert.textFields.lastObject;
        dismissHandler(txtCurrentPassword.text);
    }];
    
    confirmAction.enabled = NO;
    [alert addAction:confirmAction];
    
    
    
    [alert addTextFieldWithConfigurationHandler:^(UITextField *textField)
     {
         textField.placeholder = @"App PIN";
         textField.secureTextEntry = YES;
         
         [textField addTarget:self action:@selector(alertTextFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
     }];
    
    
    [self presentViewController:alert animated:YES completion:^{}];
}


- (void)showActionSheetForImageInputWithCompletionHandler:(AlertViewButtonClickedAtIndexHandler)completionHandler
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:@"Choose the media for your picture" preferredStyle:UIAlertControllerStyleActionSheet];
    
    [alert addAction:[UIAlertAction actionWithTitle:@"Camera" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        completionHandler(ActionSheetUserSelectionCamera);
    }]];
    
    [alert addAction:[UIAlertAction actionWithTitle:@"Photo Library" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        completionHandler(ActionSheetUserSelectionPhotoLibrary);
    }]];
    
    
    [alert addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil]];
    
    
    [self presentViewController:alert animated:YES completion:^{}];
}


- (void)showActionSheetWithPickerView:(UIPickerView *)pickerView titleMessage:(NSString *)title completionHandler:(AlertViewDismissHandler)completionHandler
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:title preferredStyle:UIAlertControllerStyleActionSheet];
    
    [alert addAction:[UIAlertAction actionWithTitle:@"Done" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        completionHandler();
    }]];
    
    
    [alert addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil]];
    
    
    [alert.view addSubview:pickerView];
    
    
    [self presentViewController:alert animated:YES completion:^{}];
}





- (void)alertTextFieldDidChange:(UITextField *)sender
{
    UIAlertController *alertController = (UIAlertController *)self.presentedViewController;
    
    if (alertController)
    {
        UITextField *login = alertController.textFields.firstObject;
        UIAlertAction *okAction = alertController.actions.lastObject;
        okAction.enabled = login.text.length > 2;
    }
}

@end
