//
//  SizeClass.m
//  Tellum
//
//  Created by Asad Ali on 08/04/2015.
//  Copyright (c) 2015 AESquares. All rights reserved.
//

#import "SizeClass.h"
#import <UIKit/UIKit.h>


@implementation SizeClass


+ (SizeClass *)currentDeviceSizes
{
    static SizeClass *sizeClass = nil;
    
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        sizeClass = [[self alloc] init];
        
        [sizeClass initializeSizes];
    });
    
    return sizeClass;
}


- (void)initializeSizes
{
    _deviceHeight = [[UIScreen mainScreen] bounds].size.height;
    _deviceWidth = [[UIScreen mainScreen] bounds].size.width;
    
    
    _isDeviceIPad = _deviceWidth == 768;
    _isDeviceIPhone4 = _deviceHeight == 480;
    _isDeviceIPhone5 = _deviceHeight == 568;
    _isDeviceIPhone6 = _deviceWidth == 375;
    _isDeviceIPhone6Plus = _deviceWidth == 414;
    
    
    _onePixelHeight = _isDeviceIPad? 0.5:_isDeviceIPhone6? 0.5:_isDeviceIPhone6Plus? 0.34:0.5;
    
    _tabbarHeight = _isDeviceIPad? 64:_isDeviceIPhone6? 35:_isDeviceIPhone6Plus? 35:35;
    
    
    _collectionViewWidth = _deviceWidth - 37;
    _collectionViewLineSpacing = 10.0;
    _collectionViewCellSpacing = 10.0;
    
    
    _collectionViewCellWidth = _collectionViewWidth;
    

    _cellImageViewPadding = 6.0;
    _cellImageViewWidth = _collectionViewCellWidth - (_cellImageViewPadding + _cellImageViewPadding);
    
    
    _cellImageViewWithImageHeight = _cellImageViewWidth;
    _cellImageViewWithoutImageHeight = _isDeviceIPad? 200:_isDeviceIPhone6? 104:_isDeviceIPhone6Plus? 120:89;
    
    _cellLogoImageWidth = _isDeviceIPad? 30:_isDeviceIPhone6? 22:_isDeviceIPhone6Plus? 28:20;
    
    _cellLabelsStandardPadding = 8.0;
    _TellumCellLabelsPadding = 10.0;
    _TellumCellDescriptionLabelWidth = _collectionViewCellWidth - (_TellumCellLabelsPadding + _TellumCellLabelsPadding);
    _TellumNewBrandCellHeight = _isDeviceIPad? 131.0:_isDeviceIPhone6? 131.0:_isDeviceIPhone6Plus? 131.0:131.0;
    _TellumCellBottomActionButtonHeight = 25.0;
    _TellumCellDescriptionLabelDefaultTopConstant = 33.0;
    _TellumCellTitleLabelHeight = 13.0;
    
    
    _autopilotCellTitleLabelPadding = _TellumCellLabelsPadding;
    _autopilotCellOptionViewsHeight = _isDeviceIPad? 30:_isDeviceIPhone6? 32.0:_isDeviceIPhone6Plus? 36.0:28.0;
    _autopilotCellOptionViewsPadding = 2.0;
    _autopilotCellTitleLabelHeight = 17.0;
    
    

    _productCellTitleLabelLeftPadding = _isDeviceIPad? 18.0:_isDeviceIPhone6? 14.5:_isDeviceIPhone6Plus? 16.0:13.0;
    _productCellLoyalityCollectionViewHeight = _isDeviceIPad? 40.0:_isDeviceIPhone6? 30.0:_isDeviceIPhone6Plus? 35.0:25.0;
    _productCellCampaignCollectionViewHeight = _isDeviceIPad? 180.0:_isDeviceIPhone6? 120.0:_isDeviceIPhone6Plus? 140.0:100.0;
    _productCellPagerViewHeight = 22.0;
    _productCellPriceLabelHeight = 11.0;
    _productCellQuantityViewHeight = _isDeviceIPad? 100:_isDeviceIPhone6? 60.0:_isDeviceIPhone6Plus? 70.0:49.0;
    _productCellSelectedPropertiesViewHeight = _isDeviceIPad? 30:_isDeviceIPhone6? 22.5:_isDeviceIPhone6Plus? 25.0:20.0;
    _productPropertyCellTitleFontSize = _isDeviceIPad? 13.0:_isDeviceIPhone6? 10.0:_isDeviceIPhone6Plus? 11.0:9.0;
    
    
    
    _personalizationFormPopupTopSpace = _isDeviceIPad? 70.0:_isDeviceIPhone6? 70.0:_isDeviceIPhone6Plus? 70.0:30.0;
    _personalizationFormPopupHeaderHeight = 50.0;
    _personalizationFormPopupStandardPadding = 20.0;
    _personalizationFormPopupTableCellHeight = _isDeviceIPad? 50.0:_isDeviceIPhone6? 41.0:_isDeviceIPhone6Plus? 41.0:41.0;
    _personalizationFormPopupSaveButtonHeight = _isDeviceIPad? 30.0:_isDeviceIPhone6? 30.0:_isDeviceIPhone6Plus? 30.0:30.0;
    _personalizationFormPopupBottomPadding = 10.0;
    
    
    
    _storeCellAddressLabelTopPadding = _TellumCellDescriptionLabelDefaultTopConstant;
    _storeCellTitleLabelHeight = _TellumCellTitleLabelHeight;
    
    
    
    _meCardsOptionViewHeight = _isDeviceIPad? 30:_isDeviceIPhone6? 24.5:_isDeviceIPhone6Plus? 40.0:22.0;
    

    _meAccountProfileImageHeight = _isDeviceIPad? 170.0:_isDeviceIPhone6? 90.0:_isDeviceIPhone6Plus? 130.0:85.0;
    
    
    _homeSearchViewHeight = _isDeviceIPad? 44.0:_isDeviceIPhone6? 44.0:_isDeviceIPhone6Plus? 44.0:44.0;
    _homePopupViewHeight = _isDeviceIPad? 300.0:_isDeviceIPhone6? 200.0:_isDeviceIPhone6Plus? 240.0:200.0;
    _homePopupViewWidth = _isDeviceIPad? 390.0:_isDeviceIPhone6? 290.0:_isDeviceIPhone6Plus? 350.0:290.0;
    
    
    _meLocationAddNewLocationViewHeight = _isDeviceIPad? 335.0:_isDeviceIPhone6? 335.0:_isDeviceIPhone6Plus? 335.0:335.0;
    _meLocationCellHeight = _isDeviceIPad? 60:_isDeviceIPhone6? 50.0:_isDeviceIPhone6Plus? 56.0:46.0;
    
    _mePaymentCardAddNewViewHeight = _isDeviceIPad? 500.0:_isDeviceIPhone6? 500.0:_isDeviceIPhone6Plus? 560.0:500.0;
    _mePaymentCellHeight = _meLocationCellHeight;
    
    
    _meLoyaltyCardCellHeight = _isDeviceIPad? 60:_isDeviceIPhone6? 50.0:_isDeviceIPhone6Plus? 56.0:46.0;
    _meLoyaltyCardCellWithoutTextHeight = _isDeviceIPad? 40.0:_isDeviceIPhone6? 35.0:_isDeviceIPhone6Plus? 38.0:30.0;
    _meLoyaltyCardLogoHeight = _isDeviceIPad? 32:_isDeviceIPhone6? 25.0:_isDeviceIPhone6Plus? 28.0:22.0;
    
    
    
    _basketLogoHeight = _isDeviceIPad? 66.0: _isDeviceIPhone6?58.0: _isDeviceIPhone6Plus?78.0:48.0;
    _basketLogoViewHeight = _isDeviceIPad? 125.0: _isDeviceIPhone6?93.0: _isDeviceIPhone6Plus?110.0:80.0;
    _basketPaymentViewHeight = _isDeviceIPad? 180.0: _isDeviceIPhone6?170.0: _isDeviceIPhone6Plus?180.0:160.0;
    _basketInvoiceTitleViewHeight = _isDeviceIPad? 55.0 : _isDeviceIPhone6?44.0 : _isDeviceIPhone6Plus?50.0:40.0;
    _basketGrandTotalViewHeight = _isDeviceIPad? 65.0 : _isDeviceIPhone6?56.0 : _isDeviceIPhone6Plus?60.0:54.0;
    _basketDeliveryChargesViewHeight = _basketGrandTotalViewHeight;
    _basketCitiCardViewHeight = _isDeviceIPad? 43.0 :_isDeviceIPhone6?40.0 : _isDeviceIPhone6Plus?43.0:35.0;
    _basketTableViewCellHeight = _isDeviceIPad? 36.0 : _isDeviceIPhone6?28.0 : _isDeviceIPhone6Plus?32.0:25.0;
    _basketTableViewCellWithPointsHeight = _basketTableViewCellHeight + 30.0;
}



@end
