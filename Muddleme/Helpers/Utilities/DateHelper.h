//
//  DateHelper.h
//  Muddleme
//
//  Created by Asad Ali on 08/04/2015.
//  Copyright (c) 2015 AESquares. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DateHelper : NSObject



+ (DateHelper *)sharedInstance;



+ (NSDate *)dateFromString:(NSString *)dateString;

+ (NSString *)stringFromDate:(NSDate *)date;


+ (NSDate *)dateFromString:(NSString *)dateString withFormat:(NSString *)dateFormat;

+ (NSString *)stringFromDate:(NSDate *)date withFormat:(NSString *)dateFormat;



@end
