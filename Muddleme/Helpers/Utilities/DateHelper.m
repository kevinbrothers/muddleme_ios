//
//  DateHelper.m
//  Muddleme
//
//  Created by Asad Ali on 08/04/2015.
//  Copyright (c) 2015 AESquares. All rights reserved.
//

#import "DateHelper.h"
#import "NSString+Helper.h"
#import "AppConstants.h"


@interface DateHelper ()


@property (nonatomic, strong) NSDateFormatter *dateFormatter;


@end



@implementation DateHelper


+ (DateHelper *)sharedInstance
{
    static DateHelper *helper = nil;
    
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        helper = [[self alloc] init];
        
        helper.dateFormatter = [[NSDateFormatter alloc] init];
        helper.dateFormatter.locale = [NSLocale systemLocale];
    });
    
    return helper;
}






+ (NSDate *)dateFromString:(NSString *)dateString
{
    DateHelper *helper = [DateHelper sharedInstance];
    
    helper.dateFormatter.dateFormat = kDateFormatStandard;
    
    return [helper.dateFormatter dateFromString:dateString.standardDateString];
}

+ (NSString *)stringFromDate:(NSDate *)date
{
    DateHelper *helper = [DateHelper sharedInstance];
    
    helper.dateFormatter.dateFormat = kDateFormatStandard;
    
    return [helper.dateFormatter stringFromDate:date];
}

+ (NSDate *)dateFromString:(NSString *)dateString withFormat:(NSString *)dateFormat
{
    DateHelper *helper = [DateHelper sharedInstance];
    
    helper.dateFormatter.dateFormat = dateFormat;
    
    return [helper.dateFormatter dateFromString:dateString];
}

+ (NSString *)stringFromDate:(NSDate *)date withFormat:(NSString *)dateFormat
{
    DateHelper *helper = [DateHelper sharedInstance];
    
    helper.dateFormatter.dateFormat = dateFormat;
    
    return [helper.dateFormatter stringFromDate:date];
}


@end
